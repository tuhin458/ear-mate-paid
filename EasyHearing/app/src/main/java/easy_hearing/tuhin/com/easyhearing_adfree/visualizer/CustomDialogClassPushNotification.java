package easy_hearing.tuhin.com.easyhearing_adfree.visualizer;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import easy_hearing.tuhin.com.easyhearing_adfree.R;


/**
 * Created by Tuhin on 10/30/2016.
 */


public class CustomDialogClassPushNotification extends Dialog implements
        View.OnClickListener {

    public Activity c;
    public Dialog d;
    public Button yes, no;

    public String pushMsg;
    public String notifyTitle;
    public Boolean BUTTON_PRESS_DETECT = false;

    public CustomDialogClassPushNotification(Activity a, String string, String title) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        pushMsg = string;
        notifyTitle = title;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialouge_push_notification);
        yes = (Button) findViewById(R.id.agree_button);

        TextView textView = (TextView) findViewById(R.id.pushMsg);
        TextView titleNotify = (TextView) findViewById(R.id.titleNotify);

        textView.setText(pushMsg.toString());
        titleNotify.setText(notifyTitle.toString());

        //  no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        //  no.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.agree_button:

                dismiss();
               // c.finish();
                break;
            /*
            case R.id.btn_no:
                dismiss();
                break;
                */
            default:
                break;
        }
        dismiss();
    }


}