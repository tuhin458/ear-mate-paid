package easy_hearing.tuhin.com.easyhearing_adfree;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.audiofx.AudioEffect;
import android.media.audiofx.Equalizer;
import android.media.audiofx.LoudnessEnhancer;
import android.media.audiofx.Visualizer;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OSNotificationReceivedResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.io.OutputStream;
import java.math.BigInteger;
import java.util.Timer;
import java.util.TimerTask;

import easy_hearing.tuhin.com.easyhearing_adfree.visualizer.CustomDialogClassPushNotification;
import io.fabric.sdk.android.Fabric;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;


public class MainActivity extends AppCompatActivity implements SeekBar.OnTouchListener, SeekBar.OnSeekBarChangeListener {

    // Map<String, CreateSeekBar> object = new HashMap<String, CreateSeekBar>();
    static AudioRecord audioRecord;
    static AudioManager audioManager;
    static AudioTrack audioTrack;
    static int classInitCount;

    int audioSessionID;

    static int loopCount = 0;
    //  CreateSeekBar seekbar_for_reset;
    static VisualizerView visualizerView;
    static Visualizer mVisualizer;
    static Record record;
    LinearLayout layoutCreateSeekbar;
    LinearLayout loudnessEnhancerHolder;
    // CreateSeekBar createSeekBar;
    Button resetEqualizer;
    static int m1;
    static int m2;
    static int m3;
    static int m4;
    static int m5;
    NotificationManager mNotificationManager;


    Bitmap mIcon_val;
    static String notificationTxt = null;
    static String notificationTitle = null;

    short putHighVal;

    BroadcastReceiver loudnessEnhanceBroadcast;
    LockableScrollView scrollLayout;

    private static final int PLUS_ONE_REQUEST_CODE = 0;
    // PlusOneButton mPlusOneButton;


    Button startListen;
    Button stopListen;
    RelativeLayout mainLayout;


    boolean open_from_notification = false;

    Activity a;


    static boolean innerIsRun;


    SeekBar loudness_enhance;


    SeekBar seekBar1;
    SeekBar seekBar2;
    SeekBar seekBar3;
    SeekBar seekBar4;
    SeekBar seekBar5;

    int putNewInt1 = -100;
    int putNewInt2 = -100;
    int putNewInt3 = -100;
    int putNewInt4 = -100;
    int putNewInt5 = -100;

    short v1;
    short v2;
    short v3;
    short v4;
    short v5;

    short vv1;
    short vv2;
    short vv3;
    short vv4;
    short vv5;

    Drawable progressDrawable;
    Drawable progressDrawableOnTouch;
    Drawable barDrawable;

    AudioEffect audioEffect;

    int getFinalLoudnessGain;

    SharedPreferences.Editor storeEqualizerDataEdit;

    SharedPreferences storeEqualizerData;
    int getSeekBarPosition_1;
    int getSeekBarPosition_2;
    int getSeekBarPosition_3;
    int getSeekBarPosition_4;
    int getSeekBarPosition_5;


    int getSeekBarPosition_loudness;

    static Equalizer equalizer;

    private void setEqualizer(SeekBar seekBar, int i) {

        Log.d("get_seekbar_id_out", String.valueOf(seekBar.getId()));

        if (innerIsRun && equalizer != null) {


            short[] bands;

            bands = equalizer.getBandLevelRange();

            final short putHighVal = bands[1];

            Log.d("number_of_band", String.valueOf(equalizer.getNumberOfBands()));
            Log.d("get_seekbar_position", String.valueOf(i));
            Log.d("get_seekbar_id", String.valueOf(seekBar.getId()));

            if (seekBar.equals(loudness_enhance)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {



                    int maxLoudnessGain = 2500;

                    getSeekBarPosition_loudness = i;

                    getFinalLoudnessGain = (i * maxLoudnessGain) / 100;

                    Log.d("seekbar_loudness", String.valueOf(getSeekBarPosition_loudness));

                    ((LoudnessEnhancer) audioEffect).setTargetGain(getFinalLoudnessGain);
                    audioEffect.setEnabled(true);
                    Log.d("seekbar_loudness", "get target gain = " + ((LoudnessEnhancer) audioEffect).getTargetGain());


                }


            }


            if (seekBar.equals(seekBar1)) {
                putNewInt1 = putNewInt1 + (2 * i);

                v1 = (short) ((putHighVal * putNewInt1) / 100);

                getSeekBarPosition_1 = i;

                try {

                    equalizer.setBandLevel((short) 0, v1);

                } catch (Exception e) {
                    Log.d("seekbar_error", e.toString());
                }
                putNewInt1 = -100;
                // v1 = 0;
            } else if (seekBar.equals(seekBar2)) {

                getSeekBarPosition_2 = i;

                putNewInt2 = putNewInt2 + (2 * i);

                v2 = (short) ((putHighVal * putNewInt2) / 100);
                equalizer.setBandLevel((short) 1, v2);
                putNewInt2 = -100;
                //  v2 = 0;
            } else if (seekBar.equals(seekBar3)) {


                getSeekBarPosition_3 = i;

                putNewInt3 = putNewInt3 + (2 * i);

                v3 = (short) ((putHighVal * putNewInt3) / 100);
                equalizer.setBandLevel((short) 2, v3);
                putNewInt3 = -100;
                //   v3 = 0;
            } else if (seekBar.equals(seekBar4)) {


                getSeekBarPosition_4 = i;

                putNewInt4 = putNewInt4 + (2 * i);

                v4 = (short) ((putHighVal * putNewInt4) / 100);
                equalizer.setBandLevel((short) 3, v4);
                putNewInt4 = -100;
                //   v4 = 0;

            } else if (seekBar.equals(seekBar5)) {


                getSeekBarPosition_5 = i;

                putNewInt5 = putNewInt5 + (2 * i);

                v5 = (short) ((putHighVal * putNewInt5) / 100);
                equalizer.setBandLevel((short) 4, v5);
                putNewInt5 = -100;
                //   v5 = 0;

            }

            Log.d("equalizer_value", String.valueOf(v1));

        } else {


            Log.d("seekbar_equalizer", "equalizer null");
        }


    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

        Log.d("seekbar_all", "seekbar all progress changed");
        if (seekBar.equals(seekBar1)) {
            Log.d("seekbar_1", "seekbar 1 progress changed");
        }

        Log.d("lockable_view", " progress changed  ");

        scrollLayout.setScrollingEnabled(false);

        try {


            Log.d("seekbar_innerRun", String.valueOf(innerIsRun));
            Log.d("seekbar_position", String.valueOf(i));

            if (equalizer == null) {

                //   equalizer = new Equalizer(1, audioSessionID);
                // equalizer.setEnabled(true);
            }


            setEqualizer(seekBar, i);

        } catch (Exception e) {
            Log.d("seekbar_error", e.toString());

        }


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

        scrollLayout.setScrollingEnabled(false);
        seekBar.setProgressDrawable(progressDrawableOnTouch);
        Log.d("lockable_view", " start tracking touch  ");


    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

        seekBar.setProgressDrawable(progressDrawable);
        // seekBar.setProgressDrawable(barDrawable);

        scrollLayout.setScrollingEnabled(true);
        Log.d("lockable_view", " stop tracking touch  ");

        storeEqualizerDataEdit = storeEqualizerData.edit();
        storeEqualizerDataEdit.putInt("store_data_1", getSeekBarPosition_1);
        storeEqualizerDataEdit.putInt("store_data_2", getSeekBarPosition_2);
        storeEqualizerDataEdit.putInt("store_data_3", getSeekBarPosition_3);
        storeEqualizerDataEdit.putInt("store_data_4", getSeekBarPosition_4);
        storeEqualizerDataEdit.putInt("store_data_5", getSeekBarPosition_5);


        storeEqualizerDataEdit.putInt("store_data_loudness_enhance", getSeekBarPosition_loudness);


        storeEqualizerDataEdit.apply();

        Log.d("seekbar_val_1", String.valueOf(getSeekBarPosition_1));
        Log.d("seekbar_val_2", String.valueOf(getSeekBarPosition_2));
        Log.d("seekbar_val_3", String.valueOf(getSeekBarPosition_3));

    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FirebaseApp.initializeApp(getApplicationContext());





        progressDrawableOnTouch = getResources().getDrawable(R.drawable.seekbar_progress_drawble_ontouch);
        progressDrawable = getResources().getDrawable(R.drawable.seekbar_progress_drawble);
        barDrawable = getResources().getDrawable(R.drawable.seekbar_drawble_ontouch);

        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        seekBar1 = findViewById(R.id.seekbar_1);
        seekBar2 = findViewById(R.id.seekbar_2);
        seekBar3 = findViewById(R.id.seekbar_3);
        seekBar4 = findViewById(R.id.seekbar_4);
        seekBar5 = findViewById(R.id.seekbar_5);


        loudness_enhance = findViewById(R.id.seekbar_loudness);


        storeEqualizerData = getSharedPreferences("ear_mate_equalizer_data", 0);


        int getSeekbarProgress_1 = storeEqualizerData.getInt("store_data_1", 50);
        int getSeekbarProgress_2 = storeEqualizerData.getInt("store_data_2", 50);
        int getSeekbarProgress_3 = storeEqualizerData.getInt("store_data_3", 50);
        int getSeekbarProgress_4 = storeEqualizerData.getInt("store_data_4", 50);
        int getSeekbarProgress_5 = storeEqualizerData.getInt("store_data_5", 50);


        int getSeekbarLoudness = storeEqualizerData.getInt("store_data_loudness_enhance", 50);


        Log.d("seekbar_val_store", String.valueOf(getSeekbarProgress_1));


        getSeekBarPosition_1 = getSeekbarProgress_1;
        getSeekBarPosition_2 = getSeekbarProgress_2;
        getSeekBarPosition_3 = getSeekbarProgress_3;
        getSeekBarPosition_4 = getSeekbarProgress_4;
        getSeekBarPosition_5 = getSeekbarProgress_5;

        getSeekBarPosition_loudness = getSeekbarLoudness;



       /*
        setEqualizer(seekBar1,getSeekbarProgress_1);
        setEqualizer(seekBar2,getSeekbarProgress_2);
        setEqualizer(seekBar3,getSeekbarProgress_3);
        setEqualizer(seekBar4,getSeekbarProgress_4);
        setEqualizer(seekBar5,getSeekbarProgress_5);
*/
        seekBar1.setProgress(getSeekbarProgress_1);
        seekBar2.setProgress(getSeekbarProgress_2);
        seekBar3.setProgress(getSeekbarProgress_3);
        seekBar4.setProgress(getSeekbarProgress_4);
        seekBar5.setProgress(getSeekbarProgress_5);

        loudness_enhance.setProgress(getSeekbarLoudness);


        if (!innerIsRun) {

            seekBar1.setEnabled(false);
            seekBar2.setEnabled(false);
            seekBar3.setEnabled(false);
            seekBar4.setEnabled(false);
            seekBar5.setEnabled(false);

            loudness_enhance.setEnabled(false);

        }


        loudness_enhance.setOnTouchListener(this);


        seekBar1.setOnTouchListener(this);
        seekBar2.setOnTouchListener(this);
        seekBar3.setOnTouchListener(this);
        seekBar4.setOnTouchListener(this);
        seekBar5.setOnTouchListener(this);

        loudness_enhance.setOnSeekBarChangeListener(this);


        seekBar1.setOnSeekBarChangeListener(this);
        seekBar2.setOnSeekBarChangeListener(this);
        seekBar3.setOnSeekBarChangeListener(this);
        seekBar4.setOnSeekBarChangeListener(this);
        seekBar5.setOnSeekBarChangeListener(this);


        // Uri.parse("ewfre")




        Toast.makeText(this, "weewt546", Toast.LENGTH_LONG);
        Log.d("test_app", "qwhduwqywq3gybr");
        a = this;

        //   onNewIntent(getIntent());

        TextView instruction = findViewById(R.id.instruction);
        boolean notification_open_bool = getIntent().getBooleanExtra("notification_opened", false);


        startListen = findViewById(R.id.start);
        stopListen = findViewById(R.id.stop);
        mainLayout = findViewById(R.id.mainLayout);



        SharedPreferences sharedPreferencesPolicy = getSharedPreferences("hearing_aid_policy", 0);
        final boolean getPolicyStatus = sharedPreferencesPolicy.getBoolean("showPolicy", false);

        final CustomDialogClass dialogClass = new CustomDialogClass(this);

        Log.d("load_notification", String.valueOf(open_from_notification));


        //////////////////////


        ////////////////////

        if (!getPolicyStatus) {
            dialogClass.show();
        }
        // preloadTimer.schedule(timerTask,1300);


        Fabric.with(this, new Crashlytics());

        Fabric.Builder builder = new Fabric.Builder(getApplicationContext());

        builder.build();

        String blspy_bannerAd = "ca-app-pub-7069979473754845/8477343412";
        String earm_bannerAd = "ca-app-pub-7069979473754845/6563761019";


        //  mPlusOneButton = (PlusOneButton) findViewById(R.id.plus_one_button);


        int checkPermissionRecAudio = ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO);


        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
                           @Override
                           public void run() {


                               runOnUiThread(new Runnable() {
                                   @Override
                                   public void run() {


                                       try {
                                           if (!notificationTxt.equals("") && !notificationTitle.equals("")) {
                                               CustomDialogClassPushNotification pushNotification = new CustomDialogClassPushNotification(a, notificationTxt, notificationTitle);
                                               pushNotification.show();
                                           }
                                       } catch (Exception e) {

                                           Log.d("OneSignalExample error", e.toString());
                                       }


                                   }
                               });

                           }
                       }

                , 2000);


        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                .init();


        String[] stringPermission = {
                android.Manifest.permission.RECORD_AUDIO,
        };




        resetEqualizer = findViewById(R.id.resetEqualizer);

        resetEqualizer.setEnabled(false);

        layoutCreateSeekbar = findViewById(R.id.seekbarHolder);
        loudnessEnhancerHolder = findViewById(R.id.loudnessEnhancerHolder);

        scrollLayout = findViewById(R.id.scrollLayout);

        visualizerView = findViewById(R.id.myvisualizerview);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);


        if (audioManager != null) {
            audioManager.setMode(AudioManager.STREAM_VOICE_CALL);
        }


        try {

            Log.d("is_class_started_id", String.valueOf(audioSessionID));

            record = new Record();




        } catch (NullPointerException e) {
            record = new Record();

            Log.d("is_class_started", "record class null");
        }


        resetEqualizer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                seekBar1.setProgress(50);
                seekBar2.setProgress(50);
                seekBar3.setProgress(50);
                seekBar4.setProgress(50);
                seekBar5.setProgress(50);

                setEqualizer(seekBar1,50);
                setEqualizer(seekBar2,50);
                setEqualizer(seekBar3,50);
                setEqualizer(seekBar4,50);
                setEqualizer(seekBar5,50);

                storeEqualizerDataEdit = storeEqualizerData.edit();
                storeEqualizerDataEdit.putInt("store_data_1", 50);
                storeEqualizerDataEdit.putInt("store_data_2", 50);
                storeEqualizerDataEdit.putInt("store_data_3", 50);
                storeEqualizerDataEdit.putInt("store_data_4", 50);
                storeEqualizerDataEdit.putInt("store_data_5", 50);


                storeEqualizerDataEdit.apply();

                scrollLayout.setScrollingEnabled(true);

            }
        });


        //  Log.d("hearing_aid_status_bool", String.valueOf(record.recordStatus()));

        try {

            BroadcastReceiver receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    boolean checkedStatus = intent.getBooleanExtra("isChecked", false);


                    Log.d("getBool", String.valueOf(checkedStatus));


                    SharedPreferences sharedPreferences = getSharedPreferences("hearing_aid_policy", 0);

                    SharedPreferences.Editor editor = sharedPreferences.edit();

                    editor.putBoolean("showPolicy", checkedStatus);

                    editor.apply();

                }
            };


            IntentFilter filter = new IntentFilter();

            filter.addAction("CustomDialogClass");

            registerReceiver(receiver, filter);
        } catch (Exception e) {

        }

        if (loopCount == 0) {
            startListen.setVisibility(View.VISIBLE);
            stopListen.setVisibility(View.INVISIBLE);

        } else if (loopCount > 0) {
            startListen.setVisibility(View.INVISIBLE);
            stopListen.setVisibility(View.VISIBLE);
        }


        startListen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startListen.setVisibility(View.INVISIBLE);
                stopListen.setVisibility(View.VISIBLE);

                // Log.d("hearing_aid_status", String.valueOf(record.classInitCount()));
                resetEqualizer.setEnabled(true);

                if (record != null) {
                    record = new Record();

                    record.start();
                }




                //  seekbar_for_reset = displayEqualizer(true, true);
                //  displayLoudnessEnhancerBar(true, true);

                // Log.d("seekbar val ", String.valueOf(m2));


                //  equalizer.setBandLevel((short) 1, v);


                //  createSeekBar.displaySeekBar();


                //  backgroundService = new BackgroundService();
                //  startService(new Intent(getApplicationContext(), BackgroundService.class));
            }
        });

        stopListen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                record.stopRecordThread();

                loopCount = 0;


                try {
                    mNotificationManager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    mNotificationManager.cancelAll();
                } catch (Exception e) {
                    Log.d("ear_err", e.toString());
                }

                if (equalizer != null) {
                    equalizer.release();
                }
                startListen.setVisibility(View.VISIBLE);
                stopListen.setVisibility(View.INVISIBLE);
                record = new Record();

                //   displayEqualizer(audioSessionID, false, true);
                //displayLoudnessEnhancerBar(audioSessionID, false, true);


//                unregisterReceiver(loudnessEnhanceBroadcast);

                //   stopService(new Intent(getApplicationContext(), BackgroundService.class));
                resetEqualizer.setEnabled(false);


            }
        });


    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        scrollLayout.setScrollingEnabled(false);
        return false;

    }





    class Record extends Thread {

        int audioSessionID;

        BluetoothProfile recordProfile;

        //  final int bufferSize = 200000;
        // final short[] buffer = new short[bufferSize];
        //  short[] readBuffer = new short[bufferSize];
        int flagChange = 0;
        short v;

        public int setPressButtonCount;


        // AudioRecord audioRecord;

        short[] bands;

        public void run() {

            innerIsRun = true;
            Process.setThreadPriority
                    (Process.THREAD_PRIORITY_URGENT_AUDIO);


            try {

                Log.d("VR", "record thread started");


                OutputStream outputStreamMp3Audio = null;

                //  audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
                int inSamplerate = 8000;

                int b = AudioTrack.getMinBufferSize(8000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);

                int minBufferRecord = AudioRecord.getMinBufferSize(8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

                try {

                    //MediaRecorder.AudioSource.VOICE_COMMUNICATION
                    //MediaRecorder.AudioSource.MIC
                    /*
                    audioRecord = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER,
                            8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, minBufferRecord);
                    */

                    audioRecord = new AudioRecord(MediaRecorder.AudioSource.CAMCORDER,
                            8000, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, minBufferRecord);

                    audioRecord.startRecording();


                } catch (Exception e) {
                    Log.d("hearing_aid", e.toString());
                }
                short[] buffer = new short[inSamplerate * 2 * 5];
                byte[] mp3buffer = new byte[(int) (7200 + buffer.length * 2 * 1.25)];


                byte[] audioData2 = new byte[minBufferRecord];


                try {

                    audioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL, 8000, AudioFormat.CHANNEL_OUT_MONO,
                            AudioFormat.ENCODING_PCM_16BIT, minBufferRecord, AudioTrack.MODE_STREAM);


                    audioSessionID = audioTrack.getAudioSessionId();

                    equalizer = new Equalizer(0, audioTrack.getAudioSessionId());

                    equalizer.setEnabled(true);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        audioEffect = new LoudnessEnhancer(audioTrack.getAudioSessionId());
                    }

                    try {

                        setEqualizer(seekBar1, getSeekBarPosition_1);
                        setEqualizer(seekBar2, getSeekBarPosition_2);
                        setEqualizer(seekBar3, getSeekBarPosition_3);
                        setEqualizer(seekBar4, getSeekBarPosition_4);
                        setEqualizer(seekBar5, getSeekBarPosition_5);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                seekBar1.setEnabled(true);
                                seekBar2.setEnabled(true);
                                seekBar3.setEnabled(true);
                                seekBar4.setEnabled(true);
                                seekBar5.setEnabled(true);

                                loudness_enhance.setEnabled(true);
                            }
                        });


                    } catch (Exception e) {

                        Log.d("seekbar_exception", e.toString());

                    }

                    Log.d("audio_session_id_inner", String.valueOf(audioSessionID));
                    Log.d("audioTrackInit", String.valueOf(audioTrack.getState()));
                } catch (Exception e) {
                    Log.d("audioTrackError", e.toString());
                }

                try

                {

                    mVisualizer = new Visualizer(audioSessionID);
                    mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
                    mVisualizer.setDataCaptureListener(
                            new Visualizer.OnDataCaptureListener() {
                                public void onWaveFormDataCapture(Visualizer visualizer,
                                                                  byte[] bytes, int samplingRate) {
                                    visualizerView.updateVisualizer(bytes);
                                }

                                public void onFftDataCapture(Visualizer visualizer,
                                                             byte[] bytes, int samplingRate) {
                                }
                            }, Visualizer.getMaxCaptureRate() / 2, true, false);


                    mVisualizer.setEnabled(true);
                } catch (Exception e) {

                    Log.d("visualize_execption", e.toString());

                }


                audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, 15, 0);


                int buffersize = minBufferRecord;
                int recBufferBytePtr = 0;
                int recBufferByteSize = minBufferRecord * 2;
                int frameByteSize = minBufferRecord / 2;
                int sampleBytes = frameByteSize;

                audioTrack.play();


                Log.d("VR", "record/play start");


                // int getBufferSizeInFrames = audioTrack.getBufferSizeInFrames();


                // SharedPreferences sharedPreferences = getSharedPreferences("equalizer", 0);

                //SharedPreferences.Editor editor = sharedPreferences.edit();


                try {


                    while (innerIsRun)

                    {


                        int local = 0;

                        Log.d("check_inside_loop1", String.valueOf(audioData2.length));
                        local = audioRecord.read(audioData2, recBufferBytePtr, minBufferRecord);
                        //  outFile.write(audioData2, 0, minBufferRecord);


                        if (loopCount <= 4) {
                            if (local > 1) {
                                loopCount++;
                            }
                        }
                        // outFile.write(audioData2);


                        int bytesRead = local;
                        Log.d("audio_buffer", "dfjwefih");
                        Log.d("check_inside_loop2", String.valueOf(audioData2.length));
                        Log.d("check_inside_loop3", String.valueOf(local));

                        //   audioData = new AudioData(audioData2);
                        audioTrack.write(audioData2, 0, local);

                    }

                    loopCount = 0;

                    try {
                        Log.d("audioRecord_state", String.valueOf(audioRecord.getState()));
                        audioTrack.stop();
                        audioRecord.stop();

                        // writeAudioBufferToSD();
                    } catch (final Exception e) {


                    }


                    // File file2 = getDir("BluetoothSpy/" + name + ".pcm", Context.MODE_PRIVATE);
                    // File file2 = getDir("BluetoothSpy/" + name + ".pcm", Context.MODE_PRIVATE);


                    //File file3 = getDir("BluetoothSpy/" + name + ".wav", Context.MODE_PRIVATE);


                    try {

                        /*
                        if (isRemoteRecordOn) {
                            File file2 = new File(Environment.getExternalStorageDirectory().getPath() + "/BluetoothSpy/" + name + ".pcm");
                            File file3 = new File(Environment.getExternalStorageDirectory().getPath() + "/BluetoothSpy/" + name + ".wav");

                            rawToWave(file2, file3);
                        }
                        */
                    } catch (Exception e) {
                        Log.d("VR_2", e.toString());
                    }

                } catch (final Exception e) {

                    Log.d("audioData_exception", e.toString());


                    /*
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("Error Report")
                            .setAction(e.toString())
                            .build());

                    */


                }

            } catch (Exception e) {

                //  Log.d("audioData_exception", e.toString());

                Log.d("VR_3", e.toString());
            }

        }

        int getRecordAudioSessionID() {
            return audioSessionID;

        }

        private boolean recordStatus() {
            boolean status = false;

            status = innerIsRun;

            return status;

        }


        @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
        public void stopRecordThread() {


            innerIsRun = false;

            loopCount = 0;
            try {
                mVisualizer.setEnabled(false);
            } catch (Exception e) {
                Log.d("easy_hear_err", e.toString());
            }
            visualizerView.destroyDrawingCache();
            audioRecord.release();
            audioTrack.release();

            audioSessionID = 0;
            // createSeekBar = null;
            // createSeekBar2 = null;

            //  audioTrack = null;
            // TextView txt = (TextView) findViewById(R.id.textView);


            try {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        seekBar1.setEnabled(false);
                        seekBar2.setEnabled(false);
                        seekBar3.setEnabled(false);
                        seekBar4.setEnabled(false);
                        seekBar5.setEnabled(false);

                        loudness_enhance.setEnabled(false);
                    }
                });
            } catch (Exception e) {

            }
        }

        public void startRecordThread() {
            innerIsRun = true;
        }


    }


    static public class NotificationExtenderBareBonesExample extends NotificationExtenderService {

        public void NotificationExtenderBareBonesExample() {

        }

        @Override
        protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
            OverrideSettings overrideSettings = new OverrideSettings();
            overrideSettings.extender = new NotificationCompat.Extender() {
                @Override
                public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
                    // Sets the background notification color to Green on Android 5.0+ devices.
                    return builder.setColor(new BigInteger("FF00FF00", 16).intValue());
                }
            };

            OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);


            notificationTxt = receivedResult.payload.body;
            notificationTitle = receivedResult.payload.title;


            Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);
            Log.d("OneSignalExample", "Notification displayed with id: " + receivedResult.payload.body);
            Log.d("OneSignalExample", "Notification displayed with id: " + receivedResult.payload.title);

            return true;
        }
    }


    public void notificationOpened(OSNotificationOpenResult openedResult) {
        OSNotification notification = openedResult.notification;
        JSONObject data = notification.payload.additionalData;
        OSNotificationAction.ActionType actionType = openedResult.action.type;

        String customKey = data.optString("customkey", null);
        if (actionType == OSNotificationAction.ActionType.ActionTaken)
            Log.i("OneSignalExample T", "Button pressed with id: " + openedResult.action.actionID);

        if (data != null)
            Log.d("OneSignalExample T", "Full additionalData:\n" + data.toString());

        Log.d("OneSignalExample T", "App in focus: " + notification.isAppInFocus);
    }


    public void notificationReceived(OSNotification notification) {

        //  String notificationTxt = notification.payload.body;

        //  CustomDialogClassPushNotification classPushNotification = new CustomDialogClassPushNotification(pushNotifyActivity,notificationTxt);

        //  classPushNotification.show();

        // Log.d("OneSignalExample back",notificationTxt);


    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        boolean notification_bool = intent.getBooleanExtra("notification_opened", false);
        Log.d("notification_bool", String.valueOf(notification_bool));

        Toast.makeText(this, String.valueOf(notification_bool), Toast.LENGTH_LONG);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


        if (innerIsRun) {

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.final_nobackground)
                            .setContentTitle("Ear Mate is running...")
                            .setContentText("Tap to open Ear Mate..");

// Creates an explicit intent for an Activity in your app
            Intent resultIntent = new Intent(this, MainActivity.class);
            resultIntent.putExtra("notification_opened", true);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);


// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(MainActivity.class);

// Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);

            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT | FLAG_CANCEL_CURRENT
                    );

            mBuilder.setContentIntent(resultPendingIntent);
            mBuilder.setOngoing(true);
            mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
// mId allows you to update the notification later on.
            mNotificationManager.notify(2008, mBuilder.build());


        }

    }


    @Override
    protected void onResume() {
        super.onResume();


        if (innerIsRun) {

            open_from_notification = true;

        }

        String url = "https://play.google.com/store/apps/details?id=easy_hearing.tuhin.com.easyhearing";

        //  mPlusOneButton.initialize(url, PLUS_ONE_REQUEST_CODE);
        ////  mPlusOneButton.setActivated(true);
        //mPlusOneButton.setEnabled(true);

        //Log.d("is_active", String.valueOf(mPlusOneButton.isActivated()));

    }
}
