package easy_hearing.tuhin.com.easyhearing_adfree;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

/**
 * Created by Tuhin on 10/30/2016.
 */


public class CustomDialogClass extends Dialog implements
        android.view.View.OnClickListener {

     private Activity c;
    public Dialog d;
    public Button yes, no;
    CheckBox cBox;

    public Boolean BUTTON_PRESS_DETECT = false;

    public CustomDialogClass(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialouge);
        yes = (Button) findViewById(R.id.agree_button);
        //  no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(this);
        //  no.setOnClickListener(this);

        setCancelable(false);
        setCanceledOnTouchOutside(false);

        cBox = (CheckBox) findViewById(R.id.checkbox);


        cBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                Intent intent = new Intent("CustomDialogClass");

                intent.putExtra("isChecked",b);

                getContext().sendBroadcast(intent);

               // Log.d("check_test", String.valueOf(b));

            }
        });



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.agree_button:


               // c.finish();
                break;
            /*
            case R.id.btn_no:
                dismiss();
                break;
                */
            default:
                break;
        }

        int checkPermissionRecAudio = ContextCompat.checkSelfPermission(c.getApplicationContext(), android.Manifest.permission.RECORD_AUDIO);
        String[] stringPermission = {
                android.Manifest.permission.RECORD_AUDIO,
        };

        if (checkPermissionRecAudio == PackageManager.PERMISSION_DENIED)

        {

            ActivityCompat.requestPermissions(c, stringPermission, 20);
        }

        dismiss();
    }


}