package easy_hearing.tuhin.com.easyhearing_adfree;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Tuhin on 9/21/2017.
 */

public class fragment_first extends Fragment {

    Activity mActivity;
    OncommunicateActivity_first mCallback;
    public interface OncommunicateActivity_first{
        void onCommunicate(View v);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mActivity = activity;

        try {
            mCallback = (OncommunicateActivity_first) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
            );
        }


    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,

                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.layout_first, container, false);

        mCallback.onCommunicate(rootView);

        return rootView;


    }


}
