/**
 * Automatically generated file. DO NOT MODIFY
 */
package easy_hearing.tuhin.com.easyhearing_adfree;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "easy_hearing.tuhin.com.easyhearing_adfree";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1000099;
  public static final String VERSION_NAME = "1.0.0";
}
